import 'package:flutter/foundation.dart';

class UserModel {
  UserModel(
      {@required this.uid,
      @required this.photoUrl,
      @required this.displayName});
  final String uid;
  final String photoUrl;
  final String displayName;
}

class MyValidators {
  final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );
  final RegExp _passwordRegExp = RegExp(
    r'^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$',
  );

  bool isValidEmail(String email) {
    return _emailRegExp.hasMatch(email);
  }
  final String invalidEmailErrorText = 'Email inválido';

  bool isValidPassword(String password) {
    return _passwordRegExp.hasMatch(password);
  }
  final String invalidPasswordErrorText = 'Contraseña inválida, debe tener al menos 6 carateres';
}

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'app/pages/home.page.dart';
import 'app/pages/login/login.page.dart';
import 'app/pages/second.page.dart';

const INITIAL_ROUTE = '/';

Route<Widget> onGenerateRoute(RouteSettings settings) {
  return MaterialPageRoute(
    settings: settings,
    builder: (context) {
      return Consumer<FirebaseUser>(
        builder: (context, user, child) {
          // if (user == null) return LoginPage.create(context);
          if (user == null) return LoginPage();
          switch (settings.name) {
            case '/':
              return HomePage();
            case '/second':
              return SecondPage();
            default:
              return Scaffold(
                body: Center(
                  child: Text('No implementado'),
                ),
              );
          }
        },
      );
    },
  );
}

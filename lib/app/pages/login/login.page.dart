import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../providers/auth_provider.dart';
import 'google_login_button.dart';


class LoginPage extends StatelessWidget {
  
  List<Widget> _buildChindren() {
    return [
      SizedBox(height: 8.0,),
      GoogleLoginButton(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    final AuthProvider model = Provider.of<AuthProvider>(context);
    return Stack(
      children: <Widget>[
        Scaffold(
          appBar: AppBar(
            title: Text('Ingresar'),
          ),
          body: Padding(
              padding: EdgeInsets.all(16.0),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: _buildChindren(),
                ),
              ),
            ),
        ),
        if(model.isLoading)
          Container(
            width: double.infinity,
            color: Colors.black54,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                  SizedBox(height: 16.0,),
                  if(model.errorMessage != '') 
                    Container(
                      width: double.infinity,
                      color: Colors.black45,
                      padding: EdgeInsets.symmetric(vertical: 8.0),
                      child: Center(
                        child: Text(model.errorMessage, 
                          style: TextStyle(
                            fontSize: 18.0,
                            color: Colors.redAccent,
                            decoration: TextDecoration.none
                          ),
                        ),
                      ), 
                    ),
                ],
              ),
            ),
          ),
      ],
    );
  }

}


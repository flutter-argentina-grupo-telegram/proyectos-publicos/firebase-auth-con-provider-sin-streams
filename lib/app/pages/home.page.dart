import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common_widgets/logout_button.component.dart';
import '../../providers/auth_provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<AuthProvider>(context);
    return auth.isLoading 
      ? Center( child: CircularProgressIndicator() )
      : Scaffold(
          appBar: AppBar(
            title: Text('Inicio'),
            actions: <Widget>[LogoutButton()],
          ),
          body: Center(
            child: RaisedButton(
              child: Text('Segunda Página'),
              onPressed: () {
                Navigator.pushNamed(context, '/second');
              },
            ),
          ),
        );
  }
}

import 'package:flutter/material.dart';

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            title: Text('Segunda'),
          ),
          body: Center(
            child: Container(
              child: Text('Segunda Página'),
            ),
          ),
        );
  }
}

import 'package:flutter/foundation.dart';

enum ProviderState {Busy, Idle}

class BaseProvider extends ChangeNotifier {
  ProviderState _state = ProviderState.Idle;

  ProviderState get state => _state;

  set state(ProviderState value) {
    this._state = value;
    notifyListeners();
  }
  
}
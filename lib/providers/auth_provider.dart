import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_ar_boilerplate/utils/my_validators.dart';

class AuthProvider extends ChangeNotifier with MyValidators{
  final _auth = FirebaseAuth.instance;
  final _google = GoogleSignIn();

  String _errorMessage = '';
  bool _isLoading = false;
  bool _submitted = false;


  String get errorMessage => _errorMessage;
  set errorMessage(String value) => updateWith(errorMessage: value);

  bool get isLoading => _isLoading;
  set isLoading(bool value) => updateWith(isLoading: value);

  bool get submitted => _submitted;
  set submitted(bool value) => updateWith(submitted: value);

  void updateWith({
    String errorMessage,
    bool isLoading,
    bool submitted,
  }) {
    this._errorMessage = errorMessage ?? this._errorMessage;
    this._isLoading = isLoading ?? this._isLoading;
    this._submitted = submitted ?? this._submitted;
    notifyListeners();
  }


  Future<void> signInWithGoogle() async {
    try {
      this.isLoading = true;

      final googleAccount = await _google.signIn();
      final googleAuth = await googleAccount.authentication;
      final credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      await _auth.signInWithCredential(credential);
      this.isLoading = false;
    } catch (e) {
      this.updateWith(isLoading: false, errorMessage: 'Falló la Autenticación');
      print(e);
    }
  }

  Future<bool> signOut() async {
    try {
      this.isLoading = true;
      await _google.signOut();
      await _auth.signOut();
      this.isLoading = false;
      return true;
    } catch (e) {
      print(e);
      this.updateWith(isLoading: false, errorMessage: 'Hubo un problema, intente salir nuevamente.');
      return false;
    }
  }

}
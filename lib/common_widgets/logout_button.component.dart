import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/auth_provider.dart';

class LogoutButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<AuthProvider>(context);
    return IconButton(
      icon: Icon(Icons.exit_to_app),
      onPressed: auth.signOut,
    );
  }
}
